# ------------------------------------------------------------------------------
# This file is a script called by the target RunClangTidyDiff to get the name of the source files of the current
# directory that differ from the origin branch. This script fills a file, which path is GIT_DIFF_DESTINATION, with the
# name of the sources files differing from the origin branch.
# ------------------------------------------------------------------------------

# An empy item in a list is considered as a regular item
cmake_policy(SET CMP0007 NEW)

# Prerequisites checking
find_program(GIT_PATH git)
if(NOT GIT_PATH)
  message(FATAL_ERROR "git not found! Aborting...")
endif()

# ------------------------------------------------------------------------------
# This macro fills the GIT_CURRENT_BRANCH variable with the name of the current git branch
# ------------------------------------------------------------------------------
macro(get_current_branch)
  execute_process(COMMAND ${GIT_PATH} rev-parse --abbrev-ref HEAD OUTPUT_VARIABLE _GIT_CURRENT_BRANCH)
  string(STRIP ${_GIT_CURRENT_BRANCH} GIT_CURRENT_BRANCH)
  message(STATUS "Current branch is  : ${GIT_CURRENT_BRANCH}")
endmacro()

# ------------------------------------------------------------------------------
# This macro fills the variable GIT_FORK_COMMIT with the commit hash that identify the fork between the current branch
# and the origin branch. If more than one commit is identified, then crashes.
# ------------------------------------------------------------------------------
macro(get_fork_commit)
  execute_process(COMMAND ${GIT_PATH} merge-base master ${GIT_CURRENT_BRANCH} OUTPUT_VARIABLE _GIT_FORK_COMMIT)
  string(REPLACE "\n" ";" GIT_FORK_COMMIT ${_GIT_FORK_COMMIT})
  list(REMOVE_ITEM GIT_FORK_COMMIT "")
  message(STATUS "Forking from commit: ${GIT_FORK_COMMIT}")
  list(LENGTH GIT_FORK_COMMIT FORK_COMMIT_NB)

  if(${FORK_COMMIT_NB} GREATER 1)
    message(FATAL_ERROR "More than one fork commit has been found! Situation not yet handled!")
  endif()
endmacro()

# ------------------------------------------------------------------------------
# This macro fills the GIT_DIFF_RES variable with every file that is different from the origin branch
# ------------------------------------------------------------------------------
macro(get_file_diff)
  execute_process(
    COMMAND ${GIT_PATH} diff --name-only ${GIT_FORK_COMMIT} ${CMAKE_SOURCE_DIR} OUTPUT_VARIABLE _GIT_DIFF_RES
  )
  string(REPLACE "\n" ";" GIT_DIFF_RES ${_GIT_DIFF_RES})
  list(REMOVE_ITEM GIT_DIFF_RES "")
endmacro()

# ------------------------------------------------------------------------------
# This macro processes the FILE_EXTENSION variable to build a regular expression stored in the FILE_PATTERN variable.
# This regular expression identifies a source file.
# ------------------------------------------------------------------------------
macro(process_file_extension)
  string(REPLACE "," "|" FILE_PATTERN ${FILE_EXTENSION})
  string(PREPEND FILE_PATTERN ".*(")
  string(APPEND FILE_PATTERN ")$")
endmacro()

# ------------------------------------------------------------------------------
# This macro get the names of the sources files that are different from the origin It writes them in the
# GIT_DIFF_DESTINATION file, which is deleted if it already exists.
# ------------------------------------------------------------------------------
macro(get_sources_diff)
  # Removes the file if present
  if(EXISTS ${GIT_DIFF_DESTINATION})
    file(REMOVE ${GIT_DIFF_DESTINATION})
  endif()

  # Get the list of every file that is different from origin
  get_file_diff()

  # Process the FILE_EXTENSION variable to make a regex that identify source file
  process_file_extension()

  # Loop over every file that is different from the origin. If it is a source file then write it in the dedicated file
  foreach(file ${GIT_DIFF_RES})
    string(REGEX MATCH ${FILE_PATTERN} code_ext ${file})
    if(code_ext)
      string(PREPEND file ${CMAKE_SOURCE_DIR} "/")
      string(APPEND file "\n")
      file(APPEND ${GIT_DIFF_DESTINATION} ${file})
      message(STATUS "Source file: ${file} is different from origin")
    endif()
  endforeach()
  message(STATUS "Sources differences have been written in ${GIT_DIFF_DESTINATION}")
endmacro()

# Main
get_current_branch()
get_fork_commit()
get_sources_diff()
