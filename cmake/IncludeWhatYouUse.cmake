# An option to enable iwyu
option(ENABLE_IWYU "Enables include-what-you-use analysis (require Debug build type)" OFF)

if(${ENABLE_IWYU})
  # Adds support for include-what-you-use
  find_program(iwyu_path NAMES include-what-you-use iwyu)
  if(${iwyu_path} STREQUAL "iwyu_path-NOTFOUND")
    message(STATUS "include-what-you-use not found!")
  else()
    message(STATUS "Found iwyu: ${iwyu_path}")
    set(CMAKE_CXX_INCLUDE_WHAT_YOU_USE "${iwyu_path};-Xiwyu;--mapping_file=${CMAKE_CURRENT_LIST_DIR}/../settings/gcc.stl.headers.imp;-Xiwyu;--mapping_file=${CMAKE_CURRENT_LIST_DIR}/../settings/qt5_11.imp;-Xiwyu;--error")
  endif()
endif()
