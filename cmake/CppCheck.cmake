# An option to enable cppcheck (only in Debug build type)
option(ENABLE_CPPCHECK "Enables code analysis through cppcheck" OFF)

if(${ENABLE_CPPCHECK})
  find_program(cppcheck_path NAMES cppcheck)
  if(${cppcheck_path} STREQUAL "cppcheck_path-NOTFOUND")
    message(FATAL_ERROR "cppcheck not found!")
  endif()
  message(STATUS "Found cppcheck: ${cppcheck_path}")
  # Disabling missingInclude as it is covered by IWYU
  # Disabling information because does not seem important
  # Suppressing shiftNegative because it triggers a lot of 
  # false positive in the vtk log macros where "<<" is used
  set(CMAKE_CXX_CPPCHECK "${cppcheck_path};--enable=warning,style,performance,portability;--disable=missingInclude,information;--suppress=shiftNegative;--error-exitcode=1;--language=c++;--std=c++17;--inline-suppr")
endif()
