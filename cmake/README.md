# Usage of CMake files

## Common steps

To use any of the CMake modules hereafter, please copy/paste the following line in the `CMakeLists.txt` of your project: 

```cmake
include(FetchContent)
FetchContent_Declare(
    common_tools
    GIT_REPOSITORY https://gitlab.com/themys/themyscommontools.git
    GIT_TAG main
)
FetchContent_MakeAvailable(common_tools)

# To have access to fetched CMake scripts
list(APPEND CMAKE_MODULE_PATH "${common_tools_SOURCE_DIR}/cmake")
```

## ClangTidy.cmake and ClangTidyDiff.cmake

### Requirements

Those modules require `clang-tidy` to be installed on your system.
Currently version 15 is explicitly required.

### Usage

To use this module in your project please add the following line in the `CMakeLists.txt` of your project:

```cmake
include(ClangTidy)
```

Once done, a new option named `ENABLE_CLANG_TIDY` is created with default value to `OFF`.
To enable `clang-tidy` analysis turn this option to `ON` and (re-)build the project. The compilation process will stop as soon as `clang-tidy` detects a problem.

Including this module implies inclusion of `ClangTidyDiff.cmake` module and creates also a compilation target named `RunClangTidyDiff`. Running this target, through `cmake --build . --target RunClangTidyDiff`, will run `clang-tidy` on the files that have been modified in the current branch of your project.

## CodeCoverage.cmake

### Requirements

This module requires `python3`, `gcov` and `gcovr` to be installed on your system.

**WARNING: Code coverage measurment implies settings compilation options that are, for now, only compatible with GNU compilers.**

### Usage

To use this module in your project please add those lines in the `CMakeLists.txt` of your project:

```cmake
include(CodeCoverage)
if(ENABLE_CODE_COVERAGE)
    message(STATUS "Registering coverage tests")
    register_coverage_targets()
endif()
```

Code coverage measurments are implemented through `ctest` tests. Those tests have to run at the end of the whole test suite.
To ensure this, you need to declare "COVERAGE_FIXTURE" as a required fixture of each of the tests of your project:

```cmake
set_tests_properties(
    test_name
    PROPERTIES
        FIXTURES_REQUIRED COVERAGE_FIXTURE
)
```

Once done, a new option named `ENABLE_CODE_COVERAGE` is created with default value to `OFF`.
To enable code coverage analysis turn this option to `ON`, (re-)build the project and run the tests.

Two files will be created at the end of the test suite, in the build directory:

- coverage.xml
- coverage.html

The html file may be viewed inside a web browser, while the xml file can be used in your IDE to visualize coverage measurments directly in the source code. For example, with `VSCode` you may want to enable `Coverage Gutters` extension.

## CppCheck.cmake

### Requirements

This module requires `cppcheck` to be installed on your system.

### Usage

To use this module in your project please add this line in the `CMakeLists.txt` of your project:

```cmake
include(CppCheck)
```

Once done, a new option named `ENABLE_CPPCHECK` is created with default value to `OFF`.
To enable `cppcheck` analysis turn this option to `ON` and (re-)build the project. The compilation process will stop as soon as `cppcheck` detects a problem.

**WARNING The target RunCppCheck has to be run after the build of the project**

## GitInfos.cmake

This file is a script called by the target `RunClangTidyDiff` of the `ClangTidyDiff.cmake` module to get the name of the source files of the current directory that differ from the origin branch.

## IncludeWhatYouUse.cmake

### Requirements

This module requires `iwyu` to be installed on your system.

### Usage

To use this module in your project please add the following line in the `CMakeLists.txt` of your project:

```cmake
include(IncludeWhatYouUse)
```

Once done, a new option named `ENABLE_IWYU` is created with default value to `OFF`.
To enable `iwyu` analysis turn this option to `ON` and (re-)build the project.
The compilation process will stop as soon as `iwyu` detects a problem.

## ListTargets.cmake

### Requirements

No specific requirements for this module.

### Usage

This module is purely a utility module. It's only purpose is to display the list of the targets declared in a directory.

To use this module in your project please add the following line in the `CMakeLists.txt` of your project:

```cmake
include(ListTargets)
```

Once done, call in the `CMakeLists.txt` of your choice the function `list_targets`, with the directory which targets should be displayed as first argument. Set the second argument to `ON` if you want to display the imported targets too.