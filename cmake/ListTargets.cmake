# This function recursively list targets of the directory in argument and of all its children
#
# ARGUMENTS
#  dir_path: path to the directory
#  imported: if TRUE the imported targets are printed too
#
function(list_targets dir_path imported)
  file(REAL_PATH ${dir_path} real_path)
  get_property(cur_bs_tgts DIRECTORY ${real_path} PROPERTY BUILDSYSTEM_TARGETS)
  get_property(cur_imp_tgts DIRECTORY ${real_path} PROPERTY IMPORTED_TARGETS)
  get_property(subdirs DIRECTORY ${real_path} PROPERTY SUBDIRECTORIES)
  if (cur_bs_tgts OR cur_imp_tgts)
    message(STATUS "Targets for directory ${real_path} are: ")
    if (cur_bs_tgts)
        message(STATUS "\tBUILDSYSTEM:")
        foreach(tgt ${cur_bs_tgts})
            message(STATUS "\t\t${tgt}")
        endforeach()
    endif()
    if (cur_imp_tgts AND imported)
        message(STATUS "\tIMPORTED:")
        foreach(tgt ${cur_imp_tgts})
            message(STATUS "\t\t${tgt}")
        endforeach()
    endif()
  endif()
  foreach(sdir ${subdirs})
      if (sdir)
        list_targets(${sdir} ${imported})
      endif()
  endforeach()
endfunction()
