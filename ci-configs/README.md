# Usage of CI YAML files

## Common steps

To use any of the YAML files of this directory please add the following section in the `.gitlab-ci.yml` file of the project:

```yaml
include:
  - "https://gitlab.com/themys/themyscommontools/raw/main/ci-configs/.clang-tidy.yml"
  - "https://gitlab.com/themys/themyscommontools/raw/main/ci-configs/.iwyu.yml"
  - "https://gitlab.com/themys/themyscommontools/raw/main/ci-configs/.cppcheck.yml"
  - "https://gitlab.com/themys/themyscommontools/raw/main/ci-configs/.check_changelog.yml"
  - "https://gitlab.com/themys/themyscommontools/raw/main/ci-configs/.builds.yml"
  - "https://gitlab.com/themys/themyscommontools/raw/main/ci-configs/.code_coverage.yml"
  - "https://gitlab.com/themys/themyscommontools/raw/main/ci-configs/.tests.yml"
  - "https://gitlab.com/themys/themyscommontools/raw/main/ci-configs/.pre-commit.yml"
```

If another branch of the `commontools` repository should be used, then replace `main` by the branch name in the previous lines.

## .builds.yml

This file furnishes three CI build sections.

- .build_debug
- .build_debug_legacy
- .build_release

Each section has to be used through the `extends` keyword of the `.gitlab-ci.yml` of the project. For example:

```yaml
gcc_debug:
  extends:
    - .build_debug
  variables:
    EXTRA_CMAKE_CONFIG_ARGS: "-DENABLE_CODE_COVERAGE=ON"

# Completes the build_debug_legacy job included in order to set code coverage
gcc_debug_legacy:
  extends:
    - .build_debug_legacy

# Completes the build_release job included in order to set doc building
gcc_release:
  extends:
    - .build_release
  variables:
    EXTRA_CMAKE_CONFIG_ARGS: "-DBUILD_DOC=ON"
```

All of three sections allow to build the project under different configurations (debug, release and debug with an old gcc version).

The definition of the `EXTRA_CMAKE_CONFIG_ARGS` variable allows to customize the `CMake` configuration step. 

The `.build_debug` section creates a `build_${CI_PROJECT_NAME}_debug` directory that is the build directory and it will be downloadable through gitlab's ci artifact system. `${CI_PROJECT_NAME}` will be replaced by the name of the project holding the `.gitlab-ci.yml`.

Similarily the `.build_release` sections creates a `build_${CI_PROJECT_NAME}_release` directory and the `.build_debug_legacy` creates a `build_${CI_PROJECT_NAME}_debug_cpp10` directory but this last one is not an artifact and thus is not available to download.

## .check_changelog.yml

This file gives access to the `.check_changelog` CI job that will check that, in the current merge request, the `CHANGELOG.md` file has been modified.

It should be used through the `extends` keyword of the `.gitlab-ci.yml` file of the project. For example:

```yaml
check_changelog:
  extends: .check_changelog
  needs: []
```

The `needs: []` line allows the job to start as soon as possible.

## .clang-tidy.yml

This file gives access to the `.clang-tidy` CI job that will compiles the project with `clang-tidy` to statically analyze the code. If any warnings or errors are found, the job will stop and fail.

It should be used through the `extends` keyword of the `.gitlab-ci.yml` file of the project. For example:

```yaml
clang-tidy:
  extends:
    - .clang-tidy
  needs: []
```

If the `CMake` configuration step has to be customized, the variable `EXTRA_CMAKE_CONFIG_ARGS` can be used.

This job will produce a codequality report artefact that will be used by `GitLab` to display informations in the code of the merge request.

## .code_coverage.yml

This file gives access to the `.code_coverage` CI job that will launch the `gcovr` command after the project has been built with code coverage on and the tests have been ran. It requires the presence of a directory named `build_${CI_PROJECT_NAME}_debug` in which the project has been built and the tests ran.

It is designed to be used through the `extends` keyword of the `.gitlab-ci.yml` file of the project in a test job. For example:

```yaml
all_tests_debug:
  extends:
    - .all_tests_debug
    - .code_coverage
  needs:
    - job: gcc_debug
      artifacts: true
```

The `needs` keyword in conjonction with `artifacts` one ensures the directory named `build_${CI_PROJECT_NAME}_debug` is present.

## .cppcheck.yml

This file gives access to the `.cppcheck` CI job that will compiles the project with `cppcheck` to statically analyze the code. If any warnings or errors are found, the job will stop and fail.

It should be used through the `extends` keyword of the `.gitlab-ci.yml` file of the project. For example:

```yaml
cppcheck:
  extends:
    - .cppcheck
  needs: []
```

If the `CMake` configuration step has to be customized, the variable `EXTRA_CMAKE_CONFIG_ARGS` can be used.

## .iwyu.yml

This file gives access to the `.iwyu` CI job that will compiles the project with `iwyu` to check for any missing or useless includes. If any warnings or errors are found, the job will stop and fail.

It should be used through the `extends` keyword of the `.gitlab-ci.yml` file of the project. For example:

```yaml
iwyu:
  extends:
    - .iwyu
  needs: []
```

If the `CMake` configuration step has to be customized, the variable `EXTRA_CMAKE_CONFIG_ARGS` can be used.

## .pre-commit.yml

This file gives access to the `.pre-commit` CI job that will runs the `pre-commit` tool on all sources of the project.

It should be used through the `extends` keyword of the `.gitlab-ci.yml` file of the project. For example:

```yaml
pre-commit:
  extends:
    - .pre-commit
  needs: []
  allow_failure: true
```

The `allow_failure: true` line allows the CI process to be successful even if the `pre-commit` job fails.

## .tests.yml

This file furnishes two CI build sections.

- .all_tests_debug
- .all_tests_release

Each section has to be used through the `extends` keyword of the `.gitlab-ci.yml` of the project. For example:

```yaml
all_tests_debug:
  extends:
    - .all_tests_debug
    - .code_coverage
  needs:
    - job: gcc_debug
      artifacts: true

all_tests_release:
  extends:
    - .all_tests_release
  needs:
    - job: gcc_release
      artifacts: true
```

Those jobs test the project under debug and release configurations, thus they require previous build jobs (gcc_debug and gcc_release) artifacts.
Moreover the `all_tests_debug` job extends `.code_coverage` so that it is able to measure code coverage (see previous section).